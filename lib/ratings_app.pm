package ratings_app;
use Dancer ':syntax';
use Dancer::Plugin::Database;
use XML::Dumper;

our $VERSION = '0.1';

get '/' => sub {
    template 'index';
};

get '/all/xml' => sub {
    my $ratings = database->quick_select( 'ratings', {} );
    content_type 'application/xml';
    return to_xml $ratings;
};

get '/all/json' => sub {
    my $ratings = database->quick_select( 'ratings', {} );
    content_type 'application/json';
    return to_json $ratings;
};

get '/individual' => sub {
    template 'instructions';
};
true;
